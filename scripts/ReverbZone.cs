﻿using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace AutoReverb {
    
    public class ReverbZone : MonoBehaviour, IAutoReverbObservable {

        [Tooltip("Set predefined reverb params")]
        public AudioReverbPreset preset = AudioReverbPreset.Generic;
        private AudioReverbPreset prevPreset = AudioReverbPreset.Generic;
        
        [Tooltip("Reverb params of this zone")]
        public ReverbParams reverbParams;
        
        private readonly List<IAutoReverbListener> listeners = new List<IAutoReverbListener>();
        private readonly int zoneId = IdProvider.GetInstance().NextId();
        

        private void OnValidate() {
            if (preset != prevPreset) {
                prevPreset = preset;
                reverbParams = ReverbParams.FromPreset(preset);
            }

            NotifyAllChanged();
        }
        
        private void OnTriggerEnter(Collider other) {
            var listener = FindInChildrenOf(other.gameObject);
            if (listener != null) Subscribe(listener);
        }

        private void OnTriggerExit(Collider other) {
            var listener = FindInChildrenOf(other.gameObject);
            if (listener != null) Unsubscribe(listener);
        }

        public void Subscribe(IAutoReverbListener listener) {
            listeners.Add(listener);
            NotifyChanged(listener);
        }

        public void Unsubscribe(IAutoReverbListener listener) {
            listeners.Remove(listener);
            NotifyRemoved(listener);
        }
        
        private void NotifyAllChanged() {
            foreach (var listener in listeners) NotifyChanged(listener);
        }

        private void NotifyChanged(IAutoReverbListener listener) {
            listener.OnZoneChanged(zoneId, reverbParams);
        }
        
        private void NotifyRemoved(IAutoReverbListener listener) {
            listener.OnZoneRemoved(zoneId);
        }

        [CanBeNull]
        private static IAutoReverbListener FindInChildrenOf(GameObject obj) {
            return obj.GetComponent<IAutoReverbListener>();
        }
        
    }

}
