﻿using UnityEngine;
using UnityEngine.Audio;

namespace AutoReverb {
    
    public abstract class AutoReverb: MonoBehaviour, IAutoReverbListener {
        
        [Tooltip("Set predefined reverb params")]
        public AudioReverbPreset reverbPreset = AudioReverbPreset.Generic;
        private AudioReverbPreset prevPreset = AudioReverbPreset.Generic;

        [Tooltip("Reverb params outside of a zone")]
        public ReverbParams reverbParams = ReverbParams.Generic();

        protected readonly AutoReverbManager Manager = new AutoReverbManager();

        
        private void OnValidate() {
            if (reverbPreset != prevPreset) {
                prevPreset = reverbPreset;
                reverbParams = ReverbParams.FromPreset(reverbPreset);
            }
            
            if (Manager.SetDefaultParams(reverbParams)) ReverbParamsChanged();
        }

        public void OnZoneChanged(int id, ReverbParams zone) {
            Manager.ChangeZone(id, zone);
            ReverbParamsChanged();
        }

        public void OnZoneRemoved(int id) {
            Manager.RemoveZone(id);
            ReverbParamsChanged();
        }

        protected virtual void ReverbParamsChanged() {}

    }
    
}