﻿namespace AutoReverb {
    
    public interface IAutoReverbListener {
        void OnZoneChanged(int id, ReverbParams zone);
        void OnZoneRemoved(int id);
    }
    
}