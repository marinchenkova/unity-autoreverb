﻿namespace AutoReverb {
    
    public class IdProvider {

        private static IdProvider instance;
        private int lastId;

        
        private IdProvider() {}
        
        public static IdProvider GetInstance() {
            return instance ?? (instance = new IdProvider());
        }

        public int NextId() {
            return lastId++;
        }

    }
    
}