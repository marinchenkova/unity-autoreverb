﻿namespace AutoReverb {
    
    public interface IAutoReverbObservable {
        void Subscribe(IAutoReverbListener listener);
        void Unsubscribe(IAutoReverbListener listener);
    }
    
}