﻿using System.Collections.Generic;

namespace AutoReverb {
    
    public class AutoReverbManager {
        
        private readonly Dictionary<int, ReverbParams> zones = new Dictionary<int, ReverbParams>();
        private ReverbParams defaultParams = ReverbParams.Generic();
        public ReverbParams Result { get; private set; } = ReverbParams.Generic();
        

        public void ChangeZone(int id, ReverbParams zone) {
            if (!zones.ContainsKey(id)) zones.Add(id, zone);
            else zones[id] = zone;
            
            CalcResult();
        }

        public void RemoveZone(int id) {
            zones.Remove(id);
            CalcResult();
        }

        public bool SetDefaultParams(ReverbParams reverbParams) {
            defaultParams = reverbParams;
            if (zones.Count != 0) return false;
            
            Result = defaultParams;
            return true;
        }
        
        private void CalcResult() {
            var size = zones.Count;
            
            if (size == 0) {
                Result = defaultParams;
                return;
            }
            
            var sum = ReverbParams.Zero();
            foreach (var p in zones) sum.Plus(p.Value);
            sum.Div(size);
            Result = sum;
        }
        
        
    }
    
}