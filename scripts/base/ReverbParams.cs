﻿using System;
using UnityEngine;

namespace AutoReverb { 
    
    [Serializable]
    public class ReverbParams {

        public static string NameDryLevel = "DryLevel";
        public static string NameRoom = "Room";
        public static string NameRoomHf = "RoomHF";
        public static string NameRoomLf = "RoomLF";
        public static string NameDecayTime = "DecayTime";
        public static string NameDecayHfRatio = "DecayHFRatio";
        public static string NameReflectionsLevel = "ReflectionsLevel";
        public static string NameReflectionsDelay = "ReflectionsDelay";
        public static string NameReverbLevel = "ReverbLevel";
        public static string NameReverbDelay = "ReverbDelay";
        public static string NameHfReference = "HfReference";
        public static string NameLfReference = "LfReference";
        public static string NameDiffusion = "Diffusion";
        public static string NameDensity = "Density";

        [Range(-10000f, 0f)] public float dryLevel;
        [Range(-10000f, 0f)] public float room;
        [Range(-10000f, 0f)] public float roomHf;
        [Range(-10000f, 0f)] public float roomLf;
        [Range(0.1f, 20f)] public float decayTime;
        [Range(0.1f, 2f)] public float decayHfRatio;
        [Range(-10000f, 1000f)] public float reflectionsLevel;
        [Range(0f, 0.3f)] public float reflectionsDelay;
        [Range(-10000f, 2000f)] public float reverbLevel;
        [Range(0f, 0.1f)] public float reverbDelay;
        [Range(1000f, 20000f)] public float hfReference;
        [Range(20f, 1000f)] public float lfReference;
        [Range(0f, 100f)] public float diffusion;
        [Range(0f, 100f)] public float density;
        
        
        public void Plus(ReverbParams other) {
            dryLevel += other.dryLevel;
            room += other.room;
            roomHf += other.roomHf;
            roomLf += other.roomLf;
            decayTime += other.decayTime;
            decayHfRatio += other.decayHfRatio;
            reflectionsLevel += other.reflectionsLevel;
            reflectionsDelay += other.reflectionsDelay;
            reverbLevel += other.reverbLevel;
            reverbDelay += other.reverbDelay;
            hfReference += other.hfReference;
            lfReference += other.lfReference;
            diffusion += other.diffusion;
            density += other.density;
        }
        
        public void Div(float divider) {
            dryLevel /= divider;
            room /= divider;
            roomHf /= divider;
            roomLf /= divider;
            decayTime /= divider;
            decayHfRatio /= divider;
            reflectionsLevel /= divider;
            reflectionsDelay /= divider;
            reverbLevel /= divider;
            reverbDelay /= divider;
            hfReference /= divider;
            lfReference /= divider;
            diffusion /= divider;
            density /= divider;
        }
        
        public void MergeWith(ReverbParams other, float weight) {
            var selfWeight = 1 - weight;
            
            dryLevel = other.dryLevel * weight + dryLevel * selfWeight;
            room = other.room * weight + room * selfWeight;
            roomHf = other.roomHf * weight + roomHf * selfWeight;
            roomLf = other.roomLf * weight + roomLf * selfWeight;
            decayTime = other.decayTime * weight + decayTime * selfWeight;
            decayHfRatio = other.decayHfRatio * weight + decayHfRatio * selfWeight;
            reflectionsLevel = other.reflectionsLevel * weight + reflectionsLevel * selfWeight;
            reflectionsDelay = other.reflectionsDelay * weight + reflectionsDelay * selfWeight;
            reverbLevel = other.reverbLevel * weight + reverbLevel * selfWeight;
            reverbDelay = other.reverbDelay * weight + reverbDelay * selfWeight;
            hfReference = other.hfReference * weight + hfReference * selfWeight;
            lfReference = other.lfReference * weight + lfReference * selfWeight;
            diffusion = other.diffusion * weight + diffusion * selfWeight;
            density = other.density * weight + density * selfWeight;
        }

        public static ReverbParams FromPreset(AudioReverbPreset preset) {
            switch (preset) {
                case AudioReverbPreset.Generic: 
                    return Generic();

                case AudioReverbPreset.PaddedCell: 
                    return PaddedCell();

                case AudioReverbPreset.Room: 
                    return Room();

                case AudioReverbPreset.Bathroom: 
                    return Bathroom();

                case AudioReverbPreset.Livingroom: 
                    return Livingroom();

                case AudioReverbPreset.Stoneroom: 
                    return Stoneroom();

                case AudioReverbPreset.Auditorium: 
                    return Auditorium();

                case AudioReverbPreset.Concerthall: 
                    return Concerthall();

                case AudioReverbPreset.Cave: 
                    return Cave();

                case AudioReverbPreset.Arena: 
                    return Arena();

                case AudioReverbPreset.Hangar: 
                    return Hangar();

                case AudioReverbPreset.CarpetedHallway: 
                    return CarpettedHallway();

                case AudioReverbPreset.Hallway: 
                    return Hallway();

                case AudioReverbPreset.StoneCorridor: 
                    return StoneCorridor();

                case AudioReverbPreset.Alley: 
                    return Alley();

                case AudioReverbPreset.Forest: 
                    return Forest();

                case AudioReverbPreset.City: 
                    return City();

                case AudioReverbPreset.Mountains: 
                    return Mountains();

                case AudioReverbPreset.Quarry: 
                    return Quarry();

                case AudioReverbPreset.Plain: 
                    return Plain();

                case AudioReverbPreset.ParkingLot: 
                    return ParkingLot();

                case AudioReverbPreset.SewerPipe: 
                    return SewerPipe();

                case AudioReverbPreset.Underwater: 
                    return Underwater();

                case AudioReverbPreset.Drugged: 
                    return Drugged();

                case AudioReverbPreset.Dizzy: 
                    return Dizzy();

                case AudioReverbPreset.Psychotic: 
                    return Psychotic();

                case AudioReverbPreset.Off:
                    return Off();

                default:
                    return Zero();
            }
        }

        public static ReverbParams Generic() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -100f,
                roomLf = 0f,
                decayTime = 1.49f,
                decayHfRatio = 0.83f,
                reflectionsLevel = -2602f,
                reflectionsDelay = 0f,
                reverbLevel = 200f,
                reverbDelay = 0.011f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }

        public static ReverbParams PaddedCell() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -6000f,
                roomLf = 0f,
                decayTime = 0.17f,
                decayHfRatio = 0.1f,
                reflectionsLevel = -1204f,
                reflectionsDelay = 0f,
                reverbLevel = 207f,
                reverbDelay = 0.002f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f,
            };
        }

        public static ReverbParams Room() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -454f,
                roomLf = 0f,
                decayTime = 0.4f,
                decayHfRatio = 0.83f,
                reflectionsLevel = -1646f,
                reflectionsDelay = 0f,
                reverbLevel = 53f,
                reverbDelay = 0.003f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }

        public static ReverbParams Bathroom() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -1200f,
                roomLf = 0f,
                decayTime = 1.49f,
                decayHfRatio = 0.54f,
                reflectionsLevel = -370f,
                reflectionsDelay = 0f,
                reverbLevel = 1030f,
                reverbDelay = 0.011f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 60f
            };
        }

        public static ReverbParams Livingroom() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -6000f,
                roomLf = 0f,
                decayTime = 0.5f,
                decayHfRatio = 0.1f,
                reflectionsLevel = -1376f,
                reflectionsDelay = 0f,
                reverbLevel = -1104f,
                reverbDelay = 0.004f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }

        public static ReverbParams Stoneroom() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -300f,
                roomLf = 0f,
                decayTime = 2.31f,
                decayHfRatio = 0.64f,
                reflectionsLevel = -711f,
                reflectionsDelay = 0f,
                reverbLevel = 83f,
                reverbDelay = 0.017f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }

        public static ReverbParams Auditorium() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -476f,
                roomLf = 0f,
                decayTime = 4.32f,
                decayHfRatio = 0.59f,
                reflectionsLevel = -789f,
                reflectionsDelay = 0f,
                reverbLevel = -289f,
                reverbDelay = 0.03f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }

        public static ReverbParams Concerthall() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -500f,
                roomLf = 0f,
                decayTime = 3.92f,
                decayHfRatio = 0.7f,
                reflectionsLevel = -1230f,
                reflectionsDelay = 0f,
                reverbLevel = -2f,
                reverbDelay = 0.029f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }

        public static ReverbParams Cave() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = 0f,
                roomLf = 0f,
                decayTime = 2.91f,
                decayHfRatio = 1.3f,
                reflectionsLevel = -602f,
                reflectionsDelay = 0f,
                reverbLevel = -302f,
                reverbDelay = 0.022f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }

        public static ReverbParams Arena() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -698f,
                roomLf = 0f,
                decayTime = 7.24f,
                decayHfRatio = 0.33f,
                reflectionsLevel = -1166f,
                reflectionsDelay = 0f,
                reverbLevel = 16f,
                reverbDelay = 0.03f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }

        public static ReverbParams Hangar() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -1000f,
                roomLf = 0f,
                decayTime = 10.05f,
                decayHfRatio = 0.23f,
                reflectionsLevel = -602f,
                reflectionsDelay = 0f,
                reverbLevel = 198f,
                reverbDelay = 0.03f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }

        public static ReverbParams CarpettedHallway() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -4000f,
                roomLf = 0f,
                decayTime = 0.3f,
                decayHfRatio = 0.1f,
                reflectionsLevel = -1831f,
                reflectionsDelay = 0f,
                reverbLevel = -1630f,
                reverbDelay = 0.03f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }

        public static ReverbParams Hallway() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -300f,
                roomLf = 0f,
                decayTime = 1.49f,
                decayHfRatio = 0.59f,
                reflectionsLevel = -1219f,
                reflectionsDelay = 0f,
                reverbLevel = 441f,
                reverbDelay = 0.011f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }

        public static ReverbParams StoneCorridor() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -237f,
                roomLf = 0f,
                decayTime = 2.7f,
                decayHfRatio = 0.79f,
                reflectionsLevel = -1214f,
                reflectionsDelay = 0f,
                reverbLevel = 395f,
                reverbDelay = 0.02f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }

        public static ReverbParams Alley() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -270f,
                roomLf = 0f,
                decayTime = 1.49f,
                decayHfRatio = 0.86f,
                reflectionsLevel = -1204f,
                reflectionsDelay = 0f,
                reverbLevel = -4f,
                reverbDelay = 0.011f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }

        public static ReverbParams Forest() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -3300f,
                roomLf = 0f,
                decayTime = 1.49f,
                decayHfRatio = 0.54f,
                reflectionsLevel = -2560f,
                reflectionsDelay = 0f,
                reverbLevel = -229f,
                reverbDelay = 0.088f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 79f,
                density = 100f
            };
        }

        public static ReverbParams City() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -800f,
                roomLf = 0f,
                decayTime = 1.49f,
                decayHfRatio = 0.67f,
                reflectionsLevel = -2273f,
                reflectionsDelay = 0f,
                reverbLevel = -1691f,
                reverbDelay = 0.011f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 50f,
                density = 100f
            };
        }

        public static ReverbParams Mountains() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -2500f,
                roomLf = 0f,
                decayTime = 1.49f,
                decayHfRatio = 0.21f,
                reflectionsLevel = -2780f,
                reflectionsDelay = 0f,
                reverbLevel = -1434f,
                reverbDelay = 0.1f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 27f,
                density = 100f
            };
        }

        public static ReverbParams Quarry() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -1000f,
                roomLf = 0f,
                decayTime = 1.49f,
                decayHfRatio = 0.83f,
                reflectionsLevel = -10000f,
                reflectionsDelay = 0f,
                reverbLevel = 500f,
                reverbDelay = 0.025f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }

        public static ReverbParams Plain() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -2000f,
                roomLf = 0f,
                decayTime = 1.49f,
                decayHfRatio = 0.5f,
                reflectionsLevel = -2466f,
                reflectionsDelay = 0f,
                reverbLevel = -1926f,
                reverbDelay = 0.1f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 21f,
                density = 100f
            };
        }

        public static ReverbParams ParkingLot() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = 0f,
                roomLf = 0f,
                decayTime = 1.65f,
                decayHfRatio = 1.5f,
                reflectionsLevel = -1363f,
                reflectionsDelay = 0f,
                reverbLevel = -1153f,
                reverbDelay = 0.012f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }

        public static ReverbParams SewerPipe() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -1000f,
                roomLf = 0f,
                decayTime = 2.81f,
                decayHfRatio = 0.14f,
                reflectionsLevel = 429f,
                reflectionsDelay = 0f,
                reverbLevel = 1023f,
                reverbDelay = 0.021f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 80f,
                density = 60f
            };
        }

        public static ReverbParams Underwater() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -4000f,
                roomLf = 0f,
                decayTime = 1.49f,
                decayHfRatio = 0.1f,
                reflectionsLevel = -449f,
                reflectionsDelay = 0f,
                reverbLevel = 1700f,
                reverbDelay = 0.011f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }

        public static ReverbParams Drugged() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = 0f,
                roomLf = 0f,
                decayTime = 8.39f,
                decayHfRatio = 1.39f,
                reflectionsLevel = -115f,
                reflectionsDelay = 0f,
                reverbLevel = 985f,
                reverbDelay = 0.03f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }

        public static ReverbParams Dizzy() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -400f,
                roomLf = 0f,
                decayTime = 17.23f,
                decayHfRatio = 0.56f,
                reflectionsLevel = -1713f,
                reflectionsDelay = 0f,
                reverbLevel = -613f,
                reverbDelay = 0.03f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }

        public static ReverbParams Psychotic() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -1000f,
                roomHf = -151f,
                roomLf = 0f,
                decayTime = 7.56f,
                decayHfRatio = 0.91f,
                reflectionsLevel = -626f,
                reflectionsDelay = 0f,
                reverbLevel = 774f,
                reverbDelay = 0.03f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 100f,
                density = 100f
            };
        }
        
        public static ReverbParams Off() {
            return new ReverbParams {
                dryLevel = 0f,
                room = -10000f,
                roomHf = -10000f,
                roomLf = 0f,
                decayTime = 1f,
                decayHfRatio = 1f,
                reflectionsLevel = -2602f,
                reflectionsDelay = 0f,
                reverbLevel = 200f,
                reverbDelay = 0.011f,
                hfReference = 5000f,
                lfReference = 250f,
                diffusion = 0f,
                density = 0f
            };
        }

        public static ReverbParams Zero() {
            return new ReverbParams {
                dryLevel = 0f,
                room = 0f,
                roomHf = 0f,
                roomLf = 0f,
                decayTime = 0f,
                decayHfRatio = 0f,
                reflectionsLevel = 0f,
                reflectionsDelay = 0f,
                reverbLevel = 0f,
                reverbDelay = 0f,
                hfReference = 0f,
                lfReference = 0f,
                diffusion = 0f,
                density = 0f
            };
        }
        
    }
    
}
