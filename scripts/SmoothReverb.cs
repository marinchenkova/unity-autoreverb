﻿using UnityEngine;
using UnityEngine.Audio;

namespace AutoReverb {
    
    public class SmoothReverb: AutoReverb {

        public DynamicMixerController mixerController;
        
        
        private void FixedUpdate() {
            mixerController.ApplyParams(Manager.Result);
        }

    }
    
}