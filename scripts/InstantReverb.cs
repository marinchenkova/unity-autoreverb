﻿using UnityEngine;
using UnityEngine.Audio;

namespace AutoReverb {
    
    public class InstantReverb: AutoReverb {

        public StaticMixerController mixerController;

        
        protected override void ReverbParamsChanged() {
            if (mixerController.reverbMixer != null) mixerController.ApplyParams(Manager.Result);
        }
        
    }
    
}