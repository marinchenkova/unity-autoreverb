﻿using System;
using UnityEngine;
using UnityEngine.Audio;

namespace AutoReverb { 
    
    [Serializable]
    public abstract class MixerController {
        
        public AudioMixer reverbMixer;
        

        public void ApplyParams(ReverbParams zone) {
            SetParam(ReverbParams.NameDryLevel, zone.dryLevel);
            SetParam(ReverbParams.NameRoom, zone.room);
            SetParam(ReverbParams.NameRoomHf, zone.roomHf);
            SetParam(ReverbParams.NameRoomLf, zone.roomLf);
            SetParam(ReverbParams.NameDecayTime, zone.decayTime);
            SetParam(ReverbParams.NameDecayHfRatio, zone.decayHfRatio);
            SetParam(ReverbParams.NameReflectionsLevel, zone.reflectionsLevel);
            SetParam(ReverbParams.NameReflectionsDelay, zone.reflectionsDelay);
            SetParam(ReverbParams.NameReverbLevel, zone.reverbLevel);
            SetParam(ReverbParams.NameReverbDelay, zone.reverbDelay);
            SetParam(ReverbParams.NameHfReference, zone.hfReference);
            SetParam(ReverbParams.NameLfReference, zone.lfReference);
            SetParam(ReverbParams.NameDiffusion, zone.diffusion);
            SetParam(ReverbParams.NameDensity, zone.density);
        }

        private void SetParam(string exposedName, float value) {
            reverbMixer.GetFloat(exposedName, out var current);
            reverbMixer.SetFloat(exposedName, CalcParam(current, value)); 
        }

        protected abstract float CalcParam(float current, float target);

    }
    
}