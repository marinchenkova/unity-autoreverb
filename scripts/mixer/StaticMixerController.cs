﻿using System;
using UnityEngine;
using UnityEngine.Audio;

namespace AutoReverb { 
    
    [Serializable]
    public class StaticMixerController : MixerController {
        
        protected override float CalcParam(float current, float target) {
            return target;
        }
        
    }
    
}