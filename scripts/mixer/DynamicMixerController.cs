﻿using System;
using UnityEngine;
using UnityEngine.Audio;

namespace AutoReverb { 
    
    [Serializable]
    public class DynamicMixerController : MixerController {
        
        [Tooltip("How fast should reverb params be changed")]
        [Range(1f, 10f)] public float applySpeed = 4f;
        

        protected override float CalcParam(float current, float target) {
            return Mathf.Lerp(current, target, Time.fixedDeltaTime * applySpeed);
        }
        
    }
    
}